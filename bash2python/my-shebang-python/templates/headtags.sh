#!/bin/bash

read -r -d '' headtags <<- EOM
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!--<meta http-equiv="refresh" content="5" >-->
      <meta http-equiv="X-UA-Compatible" content="ie=edge" />
      <!--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">-->
      <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
      <link rel = "stylesheet" type = "text/css" href = "./shared/main.css" />
      <link rel = "stylesheet" type = "text/css" href = "./shared/index.css" />
      <link rel = "stylesheet" type = "text/css" href = "./shared/bootstrap.css" />     
      <link
      rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous"/>
EOM
