#!/bin/bash

read -r -d '' footer <<- EOM
<footer class="page-footer font-small special-color-dark pt-4">

<div class="container">
   <!-- Social buttons -->
    <ul class="list-unstyled list-inline text-center">
      <li class="list-inline-item">
        <a class="btn-floating btn-fb mx-1" href="https://www.facebook.com/nicolus.rottie.9">
          <i class="fab fa-facebook-f"> </i>
        </a>
      </li>
      <li class="list-inline-item">
        <a class="btn-floating btn-tw mx-1" href="https://twitter.com/TheRotich">
          <i class="fab fa-twitter"> </i>
        </a>
      </li>
      <li class="list-inline-item">
        <a class="btn-floating btn-gplus mx-1" href="https://scholar.google.fi/citations?user=IJRM18YAAAAJ&hl=en">
          <i class="fab fa-google-plus-g"> </i>
        </a>
      </li>
      <li class="list-inline-item">
        <a class="btn-floating btn-li mx-1" href="https://linkedin.com/in/rotichtheengineer/">
          <i class="fab fa-linkedin-in"> </i>
        </a>
      </li>
    </ul>
    <!-- Social buttons -->

  </div>

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3"> Copyright &copy; <script>document.write(new Date().getFullYear())</script>:
    <a href="https://nkrtech.com/"> https://nkrtech.com/ </a>
  </div>
  <!-- Copyright -->

</footer>
EOM
