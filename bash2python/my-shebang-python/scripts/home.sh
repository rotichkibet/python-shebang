#!/bin/bash

source ./templates/nav.sh 2>/dev/null
source ./templates/footer.sh
source ./templates/headtags.sh

index_filepath=./$prName/public/index.html

cat > $index_filepath <<- _EOF_ 
 <html>
   <!-- headtags -->
    <head>
      <title>She-Bang Framework </title>
      $headtags
    </head>
  <!-- end headtags -->
  <body>
      <!-- navbar -->
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="./"><img src="./shared/img/logo.png" alt="She-Bang logo"><span class="sr-only">(current)</span></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
      <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
          $nav0 
      </ul>
      <form class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" type="search" placeholder="Search">
        <button class="btn btn-primary btn-lg" type="submit">Search</button>
      </form>
    </div>
    </nav>
      <!-- navbar -->

      <!-- app title -->
      <div class="title">
        <h2></h2>
      </div>
      <!-- app title -->

<!-- custom application start -->

<div id="hero">
  <div class="inner">
    <div class="right">
      <h1>
        Speed-Up Development<br>Bash Framework
      </h1>
      <p>
        <a id="modal-player" class="button has-icon" href="javascript:;">
          <svg aria-labelledby="simpleicons-play-icon" role="img" viewbox="0 0 100 125" fill="#FFFFFF"><title id="simpleicons-play-icon" lang="en">Play icon</title><path d="M50,3.8C24.5,3.8,3.8,24.5,3.8,50S24.5,96.2,50,96.2S96.2,75.5,96.2,50S75.5,3.8,50,3.8z M71.2,53.3l-30.8,18  c-0.6,0.4-1.3,0.5-1.9,0.5c-0.6,0-1.3-0.1-1.9-0.5c-1.2-0.6-1.9-1.9-1.9-3.3V32c0-1.4,0.8-2.7,1.9-3.3c1.2-0.6,2.7-0.6,3.8,0  l30.8,18c1.2,0.6,1.9,1.9,1.9,3.3S72.3,52.7,71.2,53.3z"/></svg>

          WHY She-Bang.SH?</a>
        <a class="button white" href="./shared/img/guide.pdf/">GET STARTED</a>
        <a class="button gray has-icon" href="https://github.com/moinonin/shebang-framework">
          <svg aria-labelledby="simpleicons-github-dark-icon" lang="" role="img" viewbox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><title id="simpleicons-github-icon" lang="en">GitHub Dark icon</title><path fill="#7F8C8D" d="M12 .297c-6.63 0-12 5.373-12 12 0 5.303 3.438 9.8 8.205 11.385.6.113.82-.258.82-.577 0-.285-.01-1.04-.015-2.04-3.338.724-4.042-1.61-4.042-1.61C4.422 18.07 3.633 17.7 3.633 17.7c-1.087-.744.084-.729.084-.729 1.205.084 1.838 1.236 1.838 1.236 1.07 1.835 2.809 1.305 3.495.998.108-.776.417-1.305.76-1.605-2.665-.3-5.466-1.332-5.466-5.93 0-1.31.465-2.38 1.235-3.22-.135-.303-.54-1.523.105-3.176 0 0 1.005-.322 3.3 1.23.96-.267 1.98-.399 3-.405 1.02.006 2.04.138 3 .405 2.28-1.552 3.285-1.23 3.285-1.23.645 1.653.24 2.873.12 3.176.765.84 1.23 1.91 1.23 3.22 0 4.61-2.805 5.625-5.475 5.92.42.36.81 1.096.81 2.22 0 1.606-.015 2.896-.015 3.286 0 .315.21.69.825.57C20.565 22.092 24 17.592 24 12.297c0-6.627-5.373-12-12-12"/></svg>
          GITHUB</a>
      </p>
    </div>
  </div>
</div>

<div id="special-sponsor">
  <h3>Official Sponsor</h3>
  
  <a href="https://inzpirogaming.com/" target="_blank">
    <img src="./shared/img/inzpiro.png" style="width:160px" alt="inzpirogaming Logo">  </a>
    <br>
    <span>With our boilerplate you can fetch your data from API endpoints without delays. Check our </span> <a href="./shared/img/guide.pdf">guide</a> for reference.
</div>


<div id="highlights">
  <div class="inner">
    <div class="point">
      <h2>Easy-Peasy</h2>
      <p class="in-brief" align="center">
        If you already know some HTML, CSS, and JavaScript, 
        this tool will get you started in no time!</p>
    </div>

    <div class="point">
      <h2>Incredibly fast</h2>
      <p class="in-brief" align="center">
        An easy developmental framework
        that scales down your work while speeding up your actual coding progress.</p>
    </div>

    <div class="point">
      <h2>Performance</h2>
      <p class="in-brief" align="center">
        The framework takes up only less than 0.3 mB on disk. It is therefore
        computationally cheap, with minimal need for configuration 
      </p>
    </div>
  </div>
</div>
<!-- custom application end -->      

  <!-- Footer Elements -->
      $footer
  <!-- Footer Elements -->
  </body>
</html>
_EOF_

