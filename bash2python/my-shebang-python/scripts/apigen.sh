#!/bin/bash

source ./variables/global.sh

# set API path to read from 
# Important! remember to enter authentication key if available

api=$(curl -s $APIPath  | jq '.')

filePath="./public/shared/api.json"

cat > $filePath <<- _EOF_

        $api

_EOF_ 

