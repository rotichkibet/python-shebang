#!/bin/bash

filepath="./templates/nav.sh"

#routepath="./public/*.html"
routepath=`ls -1 ./public/*.html 2>/dev/null | wc -l`

# Cases
# case 1

if [[ ! -f $filepath ]] && [[ ! $routepath != 0 ]]; then
  echo "Navbar will be created at $filepath ..."
  read -p "Enter navbar items (#word.html separated by spaces): "  navbar
  navbar=( "api-fetch" "${navbar[@]}" )
  echo "#!/bin/bash" >> ./templates/nav.sh
  echo "read -r -d '' nav0 <<- EOM " >> ./templates/nav.sh
  #echo "<li class="navigation"> <a class="nav-link" href="./api-fetch.html">API-Demo</a> </li>" >> ./templates/nav.sh
  navig=("")
  for route in ${navbar[@]}
  do
     basenames="${route%.*}"
     echo "<li class="navigation"><a class="nav-link" href="./${basenames[@]}.html">${basenames[@]^}</a></li>" >> ./templates/nav.sh
     navig=( "${navig[@]}" "${basenames[@]}" )
  done
 echo "EOM" >> ./templates/nav.sh
 chmod +x ./templates/nav.sh
 export var1="${navbar[@]}" 
    echo "${navig[@]}" " list items..... created in ./templates/nav.sh"
    #./scripts/home.sh 
    echo "Ready for development? execute 'npm run dev' now ..." 
# case 2
elif [[ ! -f $filepath ]] && [[ $routepath != 0 ]]; then
    echo "Existing routes will be used to build navigation bar"
	read -p "Want to continue? (yes/no): " proceed  
     if [[ $proceed == "yes" ]] || [[ $proceed == "y" ]]; then
        #rm ./public/index.html 2>/dev/null
        yourfilenames=`ls ./public/*.html` 2>/dev/null
	    routeList=""
	    for eachfile in $yourfilenames
	    do
 	      PATH=${eachfile%.*}
 	      fileName="${PATH##*/}"
 	      routeList=( "${routeList[@]}" "${fileName[@]}" $'\n' )
	    done
        navbar=${routeList[@]}
	    case "${navbar[@]%.*}" in  *"index"*) echo "index menu item is not needed consider removing" ;; esac
        echo "#!/bin/bash" >> ./templates/nav.sh
	    echo "read -r -d '' nav0 <<- EOM " >> ./templates/nav.sh
	    navig=("")
        for route in ${navbar[@]}
  	    do
        basenames="${route%.*}"
        echo "<li class="navigation"><a class="nav-link" href="./${basenames[@]}.html">${basenames[@]^}</a></li>" >> ./templates/nav.sh 
  	    navig=( "${navig[@]}" "${basenames[@]}" )
        done
	   echo "EOM" >> ./templates/nav.sh
       chmod +x ./templates/nav.sh 2>/dev/null
       export var1="${navbar}"
       echo "${navig[@]}" " list items..... created in ./templates/nav.sh"
       #./scripts/home.sh 
       echo "Ready for development? execute 'npm run regen' now ..."
     elif [[ $proceed == "no" ]] || [[ $proceed == "n" ]]; then
       echo "Automatic creation of Navbar is recommended!"
     else 
      echo "Incorrect entry! try again with the right syntax"
     fi

elif [[ -f $filepath ]] && [[ $routepath != 0 ]]; then
  echo "Navbar items and routes already exists..."
  read -p "Warning: Sure you want to overide existing? routes will be removed (yes/no) : "  remove
    if [[ $remove == "yes" ]] || [[ $remove == "y" ]]; then
    echo "... cleaning the directory"
    rm ./templates/nav.sh 
	rm ./public/*.html 
	#echo "... done! re-run the command 'npm run regen' to complete the process"
    read -p "Enter navbar items (#word.html separated by spaces): "  navbar

	echo "#!/bin/bash" >> ./templates/nav.sh
	echo "read -r -d '' nav0 <<- EOM " >> ./templates/nav.sh
    navig=("")
    for route in ${navbar[@]}
  	do
     basenames="${route%.*}"
     echo "<li class="navigation"><a class="nav-link" href="./${basenames[@]}.html">${basenames[@]^}</a></li>" >> ./templates/nav.sh
  	 navig=( "${navig[@]}" "${basenames[@]}" )
    done
	echo "EOM" >> ./templates/nav.sh
    
    chmod +x ./templates/nav.sh
    export var1="${navbar}"
    echo "${navig[@]}" " list items..... created in ./templates/nav.sh"
    #./scripts/home.sh 
    #echo "Ready for development? execute 'npm run dev' now ..."     
    
    elif [[ $remove == "no" ]] || [[ $remove == "n" ]]; then
	echo "Existing navbar will be used in the build"
    else
	echo "Incorrect entry! try running the script again with correct syntax"
    fi
elif [[ -f $filepath ]] && [[ ! $routepath != 0 ]]; then
    input="./templates/nav.sh"
    navig=("")
    while IFS='' read -r line || [[ -n "$line" ]]
    do
      IFS= read -r arr<<< "$line"
      IFS='=' read -r class className linkType  f <<< "$arr"
      IFS='>' read -r items route <<< "$f"
      IFS='/' read -r del fileExt <<< "$items"
      navig=("${navig[@]}" "${fileExt[@]}") 
    done < "$input"
    chmod +x ./templates/nav.sh
    navbar=${navig[@]}
    export var1="${navbar}"
    echo "hooray! route paths were generated from existing navbar"
    echo " ..."
    echo "Start the development by running 'npm run dev' "
else 
echo "Incorrect entry! try running the script again with correct syntax"

fi

