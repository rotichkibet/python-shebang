#!/bin/bash

#source ./templates/footer.sh
#source ./templates/headtags.sh
source ./scripts/navgen.sh
source ./scripts/home.sh

routes="${var1}"

for fileName in ${routes[@]}
do
  filename="${fileName%.*}"
  routeName="${filename[@]^}"
  filePath="./public/$filename.html"
  echo "${routeName[@]}" " route..... is found in" ${filePath[@]}

navigation="${var1%.*}"
navigs=("")

for route in ${navigation[@]}
do
	basenames="${route%.*}"
	item="<li class="navigation"><a class="nav-link" href="./${basenames[@]}.html">${filename[@]^}</a></li>" 
	navigs=( "${navigs[@]}" "${item[@]}" $'\n' )
done

cat > $filePath <<- _EOF_
<html>
<!-- headtags -->
	$headtags
<title>Insert custom page title here</title>
<!-- end headtags -->
<body>
  
 <!-- navbar -->
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="./"><img src="./shared/img/logo.png" alt="She-Bang logo"><span class="sr-only">(current)</span></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
      <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
        $nav0
      </ul>
      <form class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" type="search" placeholder="Search">
        <button class="btn btn-primary btn-lg" type="submit">Search</button>
      </form>
    </div>
    </nav>
 <!-- navbar -->
				
<!-- app title -->
	<div class="title-main">
        <h2 id="title-main"></h2>
    </div>
<!-- app title -->

<!-- custom application start -->
	<div class="card">
		<div id="content" align="center">
			[==Place your website content here==]
		</div>
	</div>

<br>

<div class="container">
  <div class="row">
    <div class="col text-center">
      <a class="button white" href="./shared/img/guide.pdf/">GET STARTED</a>
    </div>
  </div>
</div>
    <!-- custom application end -->      


<!-- Footer -->
	$footer
<!-- Footer -->
</body>
</html>
_EOF_
done

#echo "Ready for development? spin the server by running 'npm run dev'"


