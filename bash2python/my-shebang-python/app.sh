#!/usr/bin/env bash

source ./templates/nav.sh
source ./templates/footer.sh
source ./variables/global.sh
source ./templates/headtags.sh

demo_filePath="./public/api-fetch.html"

while :; do
  
  # Declare all local variables here
  pageTitle="DEMO Latest Rates Against $base as of $(date +"%x %r %Z")"
#  entryDate=$(curl -s $APIPath  | jq '.date') 
#  entryTime=$(curl -s $APIPath  | jq '.time_last_updated') 
  GBP=$(curl -s $APIPath  | jq -r '.rates.GBP') 
  USD=$(curl -s $APIPath  | jq -r '.rates.USD') 
  AUD=$(curl -s $APIPath  | jq -r '.rates.AUD') 
  SGD=$(curl -s $APIPath  | jq -r '.rates.SGD') 
  CHF=$(curl -s $APIPath  | jq -r '.rates.CHF')
  BGN=$(curl -s $APIPath  | jq -r '.rates.BGN') 
  SEK=$(curl -s $APIPath  | jq -r '.rates.SEK') 
  CAD=$(curl -s $APIPath  | jq -r '.rates.CAD') 
  NZD=$(curl -s $APIPath  | jq -r '.rates.NZD') 
  ILS=$(curl -s $APIPath  | jq -r '.rates.ILS')
  JPY=$(curl -s $APIPath  | jq -r '.rates.JPY') 
  base=$(curl -s $APIPath  | jq -r '.base') 

cat > $demo_filePath <<- _EOF_
<!DOCTYPE html>
<html lang="en">
  <!-- headtags -->
   <title> An API end-point data acquisition </title>
   <meta http-equiv="refresh" content="5" >
    $headtags
  <!-- end headtags -->
  <body>

      <!-- navbar -->
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="./"><img src="./shared/img/logo.png" alt="She-Bang logo"><span class="sr-only">(current)</span></a> 
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

      <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
          $nav0
        </ul>
      <form class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" type="search" placeholder="Search">
        <button class="btn btn-primary btn-lg" type="submit">Search</button>
      </form>
    </div>
    </nav>
      <!-- navbar -->

      <!-- app title -->
      <div class="title-main">
        <h2 id="title-main">$pageTitle</h2>
      </div>
      <!-- app title -->

    <!-- custom application start -->
      <ul class="nav justify-content-end">
        <li class="endpoint">
          <a class="nav-link" href="$APIPath"> ECB API Endpoint</a>
        </li>
      </ul>
      <div class="card">
  <table class="table table-bordered">
    <thead class="thread">
      <tr>
        <th>Currency Name</th>
        <th>Currency Symbol</th>
        <th>Real-Time Exchange Rate</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <strong><td>Great Britain Pound</td></strong>
        <td>GBP</td>
        <td>$GBP</td>
      </tr>        
        <td>US Dollar</td>
        <td>USD</td>
        <td>$USD</td>
      </tr>       
        <td>Australian Dollar</td>
        <td>AUD</td>
        <td>$AUD</td>
      </tr>         
        <td>Swiss Franc.</td>
        <td>CHF</td>
        <td>$CHF</td>
      </tr>
        <td>Swedish Krona</td>
        <td>SEK</td>
        <td>$SEK</td>
      </tr>
        <td>Canadian Dollar</td>
        <td>CAD</td>
        <td>$CAD</td>
      </tr>
        <td>New Zealand Dollar</td>
        <td>NZD</td>
        <td>$NZD</td>
      </tr>
        <td>Israel New Shekel</td>
        <td>ILS</td>
        <td>$ILS</td>
      </tr> 
        <td>Japanese Yen</td>
        <td>JPY</td>
        <td>$JPY</td>
      </tr>
    </tbody>
  </table>
</div>

<br>
<div class="container">
  <div class="row">
    <div class="col text-center">
       <a class="button white" href="./shared/img/guide.pdf/">GET STARTED</a>
    </div>
  </div>
</div>
    <!-- custom application end -->      


    <!-- Footer -->
      $footer
    <!-- Footer -->
  </body>
</html>
_EOF_
done
sleep $delay
rm $demo_filePath
